﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {
    public EventSystem EventSystem;
    public GameObject SelectedGameObject;

    private bool buttonSelected;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Math.Abs(Input.GetAxisRaw("Vertical")) > 0 && buttonSelected == false) {
            EventSystem.SetSelectedGameObject(SelectedGameObject);
            buttonSelected = true;
        }
	}

    private void OnDisable()
    {
        buttonSelected = false;
    }
}
