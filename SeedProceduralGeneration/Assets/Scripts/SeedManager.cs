﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedManager : MonoBehaviour
{
    public int seed = 0;
    public int mapWidth = 1000;
    public int mapHeight = 1000;
    public int roomMinSize = 3;
    public int roomMaxSize = 20;
    public int numOfRooms = 20; // Temporary, this will
     
    // Start is called before the first frame update
    void Awake()
    {
        Random.InitState(seed);
        // TODO: Loop through and make random room sizes (width, and length)
        Debug.Log($"Map is {mapWidth} x {mapHeight}");

        // TODO: Get a room x, y, width, height
        // TODO: Check if room collides with an existing room
        // TODO: (add 1 cell for padding between rooms)
        // TODO: Max rooms is determined by how many max sized rooms will fit
        // TODO: 1000x1000, if max room size is 20, 1000/20 = 50 rooms width-wize
        // TODO: if max room size is 20, 1000/20 = 50. 50 x 50 = 2500 rooms

        for (int i = 0; i < numOfRooms; i++)
        {
            var width = Random.Range(roomMinSize, roomMaxSize); // These values can be set higher up
            var height = Random.Range(roomMinSize, roomMaxSize); // These values can be set higher up
            var x = Random.Range(0, mapWidth-1);
            var y = Random.Range(0, mapHeight - 1);
            // Check if room collides with another
            Debug.Log($"Room {i+1}, ({x},{y}) W: {width}, H: {height}");
        }
    }
}
