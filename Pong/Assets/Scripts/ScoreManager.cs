﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public GameObject Player1Score;
    public GameObject Player2Score;
    public static ScoreManager instance = null;
    private int _player1Score = 0, _player2Score = 0;

    public void Awake()
    {
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    private void Update()
    {
        var player1Text = Player1Score.GetComponent<Text>();
        player1Text.text = _player1Score.ToString();
        var player2Text = Player2Score.GetComponent<Text>();
        player2Text.text = _player2Score.ToString();
    }

    public void GiveScore(Player player, int score)
    {
        switch (player)
        {
            case Player.Player1:
                _player1Score += score;
                break;
            case Player.Player2:
                _player2Score += score;
                break;
        }
    }
}

public enum Player
{
    Player1,
    Player2
}
