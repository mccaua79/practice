﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovement : MonoBehaviour
{
    public float Speed;
    private Rigidbody2D _rb2d;
    // Start is called before the first frame update
    void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //var move = Input.GetAxisRaw("Horizontal");
        var move = Input.GetAxisRaw("Vertical");

        _rb2d.velocity = new Vector2(0f, move * Speed);
    }
}
