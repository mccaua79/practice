﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoundController : MonoBehaviour
{
    public Player Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "ball")
        {
            ScoreManager.instance.GiveScore(Player, 1);
            collision.gameObject.GetComponent<BallController>().ResetBall();
        }
    }
}
