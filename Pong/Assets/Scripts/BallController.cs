﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float ReleaseForce;

    private Rigidbody2D _rb2d;

    public void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        Invoke("GoBall", 2);
    }
    public void GoBall()
    {
        transform.parent = null;
        float rand = Random.Range(0, 2);
        if (rand < 1)
        {
            _rb2d.AddForce(new Vector2(ReleaseForce, -ReleaseForce*.75f));
        }
        else
        {
            _rb2d.AddForce(new Vector2(-ReleaseForce, -ReleaseForce*0.75f));
        }
    }

    public void ResetBall()
    {
        _rb2d.velocity = Vector2.zero;
        transform.position = Vector2.zero;
    }
}
