﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour {
    public float Speed = 0;
    public static BackgroundScroller Current;

    private float pos = 0;
    private Renderer _renderer;

	// Use this for initialization
	void Start () {
        Current = this;
        _renderer = GetComponent<Renderer>();
	}
	
    public void Go() {
        pos += Speed;
        if (pos > 1.0f) {
            pos -= 1.0f;
        }

        _renderer.material.mainTextureOffset = new Vector2(pos, 0);
    }
}
